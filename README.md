# Circle Jump Package

This repo provides an example Godot package for the Ubuntu Touch
[Godot app template](https://gitlab.com/clickable/ut-app-meta-template/).
Find source code [here](https://github.com/kidscancode/circle_jump).
